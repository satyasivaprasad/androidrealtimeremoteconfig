package com.m.mycloudfunctionsdemo

import android.util.Log
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage


class MyCloudFunctionsFirebaseMessagingService : FirebaseMessagingService() {
    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
// [START receive_message]
    override fun onMessageReceived(remoteMessage: RemoteMessage) { // [START_EXCLUDE]
        Log.v("Firebase", "Refresh Remote Config")
        // Check if message contains a data payload.
        if (remoteMessage.data != null && remoteMessage.data.isNotEmpty()) {

        }
    }


    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    override fun onNewToken(token: String) {
        Log.v("Firebase", "Firebase onNewToken")
        FirebaseMessaging.getInstance().subscribeToTopic("PUSH_RC");
    }

    /**
     * Handle time allotted to BroadcastReceivers.
     */
    private fun handleNow() {
    }

    /**
     * Persist token to third-party servers.
     *
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private fun sendRegistrationToServer(token: String) { // TODO: Implement this method to send token to your app server.

    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private fun sendNotification(messageBody: String) {

    }

}