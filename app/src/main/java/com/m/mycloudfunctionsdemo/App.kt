package com.m.mycloudfunctionsdemo

import android.app.Application
import com.google.firebase.FirebaseApp
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings

class App : Application() {
    override fun onCreate() {
        super.onCreate()
        initFirebase()
    }
    private fun initFirebase() {
        FirebaseApp.initializeApp(this)
        val firebaseRemoteConfig = FirebaseRemoteConfig.getInstance()
        val configSettings = FirebaseRemoteConfigSettings.Builder()
            .setFetchTimeoutInSeconds(0)
            .setMinimumFetchIntervalInSeconds(0)
            .build()
        firebaseRemoteConfig?.setConfigSettingsAsync(configSettings)
        FirebaseMessaging.getInstance().subscribeToTopic("PUSH_RC")
    }
}